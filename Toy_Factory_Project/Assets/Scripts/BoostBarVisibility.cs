﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoostBarVisibility : MonoBehaviour {

	public PlayerMovement playerMovement;
	public GameObject boostBarSlider;

	void Update () 
	{

		if (playerMovement.currentPlayerNumber == 2)
		{
			boostBarSlider.SetActive(true);
		}
		else
		{
			boostBarSlider.SetActive(false);
		}
	}
}
