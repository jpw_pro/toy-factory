﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoostScript : MonoBehaviour {
	
	public float boostTimeStart;
	public float boostTimeEnd;
	public float sliderKeyUp;
	public float sliderKeyDown;
	public float playerSpeedPrev;
	public float boost;

	public bool sliderFull;
	public bool directionKeyPressed;
	public bool canBoost;

	public GameObject sliderColourObject;

	public Slider boostSlider;

	public Image sliderColour;

	public PlayerMovement playerMovement;
	
	// Use this for initialization
	void Start () 
	{

		sliderColour = sliderColourObject.GetComponent<Image> ();
	}
	
	void Update ()
	{
		if (playerMovement.currentPlayerNumber == 2)
		{
			if (Input.GetKeyDown (KeyCode.LeftShift))
			{
				boostTimeStart = Time.time;
				sliderKeyDown = boostSlider.value;
			}

			if (Input.GetKeyUp (KeyCode.LeftShift)) 
			{
				boostTimeEnd = Time.time;
				sliderKeyUp = boostSlider.value;
				directionKeyPressed = false;
			}
		
			if (((Input.GetKey (KeyCode.W)) || (Input.GetKey (KeyCode.A)) || (Input.GetKey (KeyCode.S)) || (Input.GetKey (KeyCode.D))) && (Input.GetKey (KeyCode.LeftShift))) 
			{
				directionKeyPressed = true;
			}

			if (((Input.GetKeyDown (KeyCode.W)) || (Input.GetKeyDown (KeyCode.A)) || (Input.GetKeyDown (KeyCode.S)) || (Input.GetKeyDown (KeyCode.D))) && (Input.GetKey (KeyCode.LeftShift))) 
			{
				boostTimeStart = Time.time;
				sliderKeyDown = boostSlider.value;
			}

			if (((Input.GetKeyUp (KeyCode.W)) || (Input.GetKeyUp (KeyCode.A)) || (Input.GetKeyUp (KeyCode.S)) || (Input.GetKeyUp (KeyCode.D))) && (Input.GetKey (KeyCode.LeftShift))) 
			{
				directionKeyPressed = false;
				boostTimeEnd = Time.time;
				sliderKeyUp = boostSlider.value;
			}
			
				Boost (KeyCode.LeftShift);
		}
		else
		{
			boostSlider.value = 3f;
		}
	}
	
	public void Boost (KeyCode keyCode)
	{
		if ((Input.GetKey (keyCode)) && (canBoost == true) && directionKeyPressed)
		{
			playerMovement.canBoost = true;
			boostSlider.value = sliderKeyDown - ((Time.time - boostTimeStart)*3f);

			if (boostSlider.value == 0f) 
			{
				canBoost = false;
				directionKeyPressed = false;
				boostTimeEnd = Time.time;
				sliderKeyUp = boostSlider.value;
			}
		}
		else
		{
			if (boostSlider.value == 3f)
			{
				sliderFull = true;
				canBoost = true;
				sliderKeyDown =boostSlider.value;
				boostTimeStart = Time.time;
				
			}

			playerMovement.canBoost = false;

			if (boostSlider.value < 3f)
			{
				sliderFull = false;
				boostSlider.value = sliderKeyUp + ((Time.time - boostTimeEnd));

				if ((boostSlider.value == 3f) && sliderFull ==false)
				{
					boostTimeStart = Time.time;
					sliderKeyDown =boostSlider.value;
					sliderFull = true;								
					canBoost = true;
				}
			}
		}

		if (canBoost)
		{
			sliderColour.color = Color.yellow;
		}

		if (canBoost == false)
		{
			sliderColour.color = Color.red;
		}
	}
}
