﻿using UnityEngine;
using System.Collections;

public class CameraReferenceScript : MonoBehaviour {

	public int playerNumber;
	public PlayerMovement playerMovement;
	public StretchScript stretchScript;
	public GameObject cameraOffsetX;

	void Update () 
	{
		if (playerMovement.playerRB[playerNumber] != null)
		{
			if (playerNumber == 3)
			{
				transform.position = new Vector3 (cameraOffsetX.transform.position.x, playerMovement.playerRB [3].gameObject.GetComponent<PlayerScript>().raycastPositionObject.transform.position.y, 0);
			}
			else
			{
				transform.position = new Vector3 (playerMovement.playerRB [playerNumber].gameObject.GetComponent<PlayerScript>().raycastPositionObject.transform.position.x , playerMovement.playerRB [playerNumber].gameObject.GetComponent<PlayerScript>().raycastPositionObject.transform.position.y, 0);
			}
		}
	}
}
