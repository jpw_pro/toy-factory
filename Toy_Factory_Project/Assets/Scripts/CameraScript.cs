﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public float cameraDistance;
	public float extraSpeed;
	public float speedMultiplier;
	public PlayerMovement playerMovement;
	public GameObject[] cameraReference;

	void Update () 
	{
		CameraMovement ();
	}

	public void CameraMovement ()
	{
		float distance = Vector3.Distance (new Vector3 (playerMovement.playerRB [playerMovement.currentPlayerNumber].transform.position.x, playerMovement.playerRB [playerMovement.currentPlayerNumber].transform.position.y, 0) , new Vector3 (transform.position.x, transform.position.y, 0));
	
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (cameraReference[playerMovement.currentPlayerNumber].transform.position.x, cameraReference[playerMovement.currentPlayerNumber].transform.position.y, cameraDistance), ((((distance*distance)*speedMultiplier)+extraSpeed)*Time.deltaTime));
	}
}
