﻿using UnityEngine;
using System.Collections;

public class EscMenuScript : MonoBehaviour {

	public bool disableMenu;
	private bool escMenuActive = false;
	public GameObject escMenu;

	void Update () 
	{
		if (disableMenu == false)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (escMenuActive)
				{
					escMenu.SetActive(false);
					escMenuActive = false;
				}
				else
				{
					escMenu.SetActive(true);
					escMenuActive = true;
				}
			}
		}
	}

	public void LoadMainMenu()
	{
		Application.LoadLevel (0);
	}

	public void ExitGame()
	{
		Application.Quit ();
	}
}
