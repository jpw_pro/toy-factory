﻿using UnityEngine;
using System.Collections;

public class GameControlScript : MonoBehaviour {
	
	public int goalsInLevel;
	public int goalsComplete = 0;

	public SaveGame saveGame;

	void Update () 
	{
		if (goalsComplete == goalsInLevel)
		{
			if (saveGame.saveGameNumber < Application.loadedLevel)
			{
				saveGame.saveGameNumber = (Application.loadedLevel);
				saveGame.WriteSaveGame();
			}

			Application.LoadLevel(Application.loadedLevel+1);
		}
	}
}
