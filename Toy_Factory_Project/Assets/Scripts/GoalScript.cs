﻿using UnityEngine;
using System.Collections;

public class GoalScript : MonoBehaviour {

	public int goalNumber;
	public GameControlScript gameControlScript;

	public void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if ((other.gameObject.GetComponent<PlayerScript>() != null) && (other.gameObject.GetComponent<PlayerScript>().playerNumber == goalNumber) && (other.gameObject.GetComponent<PlayerScript>().pickupObtained))
			{
				gameControlScript.goalsComplete++;
			}
			else if ((other.gameObject.GetComponentInParent<PlayerScript>().playerNumber == goalNumber) && (other.gameObject.GetComponentInParent<PlayerScript>().pickupObtained))
			{
				gameControlScript.goalsComplete++;
			}
		}
	}

	public void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if ((other.gameObject.GetComponent<PlayerScript>().playerNumber == goalNumber) && (other.gameObject.GetComponent<PlayerScript>().pickupObtained))
			{
				gameControlScript.goalsComplete--;
			}
		}
	}
}
