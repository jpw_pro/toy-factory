﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HazardScript : MonoBehaviour {

	public string deathMessage;
	public Text deathText;
	public GameObject deathCanvas;

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == ("Player"))
		{
			deathText.text = (other.gameObject.GetComponent<PlayerScript>().playerName + " " + deathMessage);
			deathCanvas.SetActive(true);
		}
	}
}
