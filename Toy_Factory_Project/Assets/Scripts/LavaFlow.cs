﻿using UnityEngine;
using System.Collections;

public class LavaFlow : MonoBehaviour {

	public Renderer lavaRenderer;
	public float scrollSpeedX;
	public float scrollSpeedY;
	public float scrollDistanceX;
	public float scrollDistanceY;
	private float randomStartOffsetX;
	private float randomStartOffsetY;


	void Awake()
	{
		randomStartOffsetX = Random.Range (0f, 100f);
		randomStartOffsetY = Random.Range (0f, 100f);
	}

	void FixedUpdate () 
	{
		float offsetX = (Mathf.Sin((randomStartOffsetX + Time.time) * scrollSpeedX))*scrollDistanceX;
		float offsetY = (Mathf.Sin((randomStartOffsetY + Time.time) * scrollSpeedY))*scrollDistanceY;
		lavaRenderer.material.SetTextureOffset("_MainTex", new Vector2((randomStartOffsetX + offsetX), (randomStartOffsetY + offsetY)));
	}
}
