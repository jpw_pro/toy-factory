﻿using UnityEngine;
using System.Collections;

public class LoadLevelNumberButton : MonoBehaviour {

	public int levelNumber;

	public void LoadLevelNumber()
	{
		Application.LoadLevel (levelNumber+1);
	}
}
