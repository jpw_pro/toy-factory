﻿using UnityEngine;
using System.Collections;

public class PlayerIndicatorScript : MonoBehaviour {
	
	public PlayerMovement playerMovement;
	public float offsetY;
	public float offsetZ;

	void Update () 
	{
		gameObject.transform.position = new Vector3 (playerMovement.playerRB [playerMovement.currentPlayerNumber].GetComponent<PlayerScript> ().raycastPositionObject.transform.position.x, playerMovement.playerRB [playerMovement.currentPlayerNumber].GetComponent<PlayerScript> ().raycastPositionObject.transform.position.y + offsetY, offsetZ);
	}
}
