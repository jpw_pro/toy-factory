﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	
	public Rigidbody[] playerRB;
	public float[] currentMovementForce;
	public float[] groundMovementForce;
	public float[] airMovementForce;
	public float[] jumpForce;
	public int[] direction;
	public bool[] canJump;
	public bool[] rotate;

	public float sprintSpeed;
	public float tempSpeed;
	public float rotateSpeed;
	public int timesJumped = 0;
	public int currentPlayerNumber = 0;
	public bool canBoost;

	void FixedUpdate () 
	{
		PlayerInput ();

		PlayerSpeed ();

		PlayerDrag ();

		for (int i = 0; i<=(rotate.Length-1); i++)
		{
			if (rotate[i])
			{
				PlayerRotation (i);
			}
		}	
	}
	
	public void PlayerInput ()
	{
		
		if (Input.GetKey (KeyCode.A))
		{
			playerRB[currentPlayerNumber].AddForce (Vector3.left * currentMovementForce[currentPlayerNumber]);
			rotate[currentPlayerNumber] = true;
			direction[currentPlayerNumber] = 1;
		}

		if (Input.GetKey (KeyCode.S))
		{
			playerRB[currentPlayerNumber].AddForce (Vector3.down * currentMovementForce[currentPlayerNumber]);
		}

		if (Input.GetKey (KeyCode.D))
		{
			playerRB[currentPlayerNumber].AddForce (Vector3.right * currentMovementForce[currentPlayerNumber]);
			rotate[currentPlayerNumber] = true;
			direction[currentPlayerNumber] = -1;
		}

		if ((Input.GetKeyDown (KeyCode.Space))&& (canJump[currentPlayerNumber]))
		{
			playerRB[currentPlayerNumber].velocity = new Vector3 (playerRB[currentPlayerNumber].velocity.x, 0, playerRB[currentPlayerNumber].velocity.z);
			playerRB[currentPlayerNumber].AddForce (Vector3.up * jumpForce[currentPlayerNumber], ForceMode.Impulse);

			if (currentPlayerNumber == 1)
			{
				timesJumped++;

				if (timesJumped >= 2)
				{
				canJump[currentPlayerNumber] = false;
				timesJumped = 0;
				}
			}
			else
			{
				canJump[currentPlayerNumber] = false;
			}
		}

		if ((currentPlayerNumber == 2) && (Input.GetKeyDown(KeyCode.LeftShift))&& canBoost)
		{
			tempSpeed = currentMovementForce[currentPlayerNumber];
			currentMovementForce[currentPlayerNumber] = sprintSpeed;
		}

		if (((currentPlayerNumber == 2) && (Input.GetKeyUp(KeyCode.LeftShift)))||canBoost == false)
		{
			currentMovementForce[currentPlayerNumber] = tempSpeed;
		}
	}	
	
	public void PlayerRotation(int playerNumber)
	{
		print ("rotationTest "+playerNumber);
		playerRB[playerNumber].transform.Rotate(0,(direction[playerNumber] * rotateSpeed * Time.deltaTime),0);

		if (playerRB[playerNumber].transform.eulerAngles.y<90)
		{
			playerRB[playerNumber].transform.eulerAngles = new Vector3 (0,90,0);

			rotate[currentPlayerNumber] = false;
		}

		if (playerRB[playerNumber].transform.eulerAngles.y>270)
		{
			playerRB[playerNumber].transform.eulerAngles = new Vector3 (0,270,0);

			rotate[currentPlayerNumber] = false;
		}
	}

	public void PlayerSpeed()
	{
		if (canJump[currentPlayerNumber])
		{
			if (canBoost)
			{
				currentMovementForce[currentPlayerNumber] = (groundMovementForce[currentPlayerNumber] + sprintSpeed);
			}
			else
			{
			currentMovementForce[currentPlayerNumber] = groundMovementForce[currentPlayerNumber];
			}
		}
		else
		{
			currentMovementForce[currentPlayerNumber] = airMovementForce[currentPlayerNumber];
		}
	}

	public void PlayerDrag ()
	{
		if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.A)) || (Input.GetKey(KeyCode.S)) || (Input.GetKey(KeyCode.D)) || (Input.GetKey(KeyCode.Space)))
		{
			playerRB[currentPlayerNumber].drag = 0.2f;
		}
		else
		{
			playerRB[currentPlayerNumber].drag = 1;
		}
	}
}
