﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public int playerNumber;
	public string playerName;
	public PlayerMovement playerMovement;
	public bool pickupObtained;
	public GameObject pickupItem;
	public GameObject raycastPositionObject;

	void Start ()
	{
		playerMovement = GameObject.FindWithTag("PlayerMovementManager").GetComponent<PlayerMovement>();
	}

	void Update () 
	{
		RayCastJumping ();
	}

	public void RayCastJumping ()
	{
		RaycastHit hit;
		Debug.DrawRay (raycastPositionObject.transform.position, Vector3.down, Color.green);

		if (Physics.SphereCast(raycastPositionObject.transform.position, 0.45f, Vector3.down, out hit, 0.6f))
		{

			if ((playerMovement.currentPlayerNumber == playerNumber) && ((hit.collider.tag == "Ground") || (hit.collider.tag == "Player") || (hit.collider.tag == "Enemy")))
			{
				playerMovement.canJump[playerNumber] = true;
			}
			else if (playerNumber != 1)
			{
				playerMovement.canJump[playerNumber] = false;
			}
		}
		else if (playerNumber != 1)
		{
			playerMovement.canJump[playerNumber] = false;
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Pickup")
		{
			if (other.gameObject.GetComponent<PickupScript>().pickupNumber == playerNumber)
			{
				pickupObtained = true;
				other.gameObject.SetActive(false);
				pickupItem.SetActive(true);
			}
		}
	}
}
