﻿using UnityEngine;
using System.Collections;

public class PlayerSwitcher : MonoBehaviour {
	
	public PlayerMovement playerMovement;
	public CameraScript cameraScript;
	public bool playerRBCheckLoop = false;
	public int tempNumber;
	public int playersInLevel;
	public int firstPlayer;
	public int lastPlayer;
	
	void Update () 
	{
		KeyInput ();
	}

	public void KeyInput ()
	{
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			if (playerMovement.currentPlayerNumber == lastPlayer)
			{
				playerMovement.currentPlayerNumber = firstPlayer;
			}
			else
			{
				if (playerMovement.currentPlayerNumber < (playersInLevel))
				{
					tempNumber = playerMovement.currentPlayerNumber;
					playerRBCheckLoop = true;

					for (tempNumber++; playerRBCheckLoop; tempNumber++)
					{
						if (playerMovement.playerRB[tempNumber] != null)
						{
							playerRBCheckLoop = false;
							playerMovement.currentPlayerNumber = tempNumber;
						}
					}
				}
				else
				{
					playerMovement.currentPlayerNumber = firstPlayer;
				}
			}
		}
		cameraScript.gameObject.transform.parent = cameraScript.cameraReference[playerMovement.currentPlayerNumber].transform;
	}
}
