﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SaveGame : MonoBehaviour {

	public StreamReader streamReader;
	public StreamWriter streamWriter;
	public int saveGameNumber;

	public StartScreen startScreen;
	public SelectLevel selectLevel;

	void Start () 
	{
		ReadSaveGame ();

		if (Application.loadedLevel == 0)
		{
			startScreen.SetButtons ();
		}

		if (Application.loadedLevel == 1)
		{
			selectLevel.CheckSavedLevels();
		}
	}

	public void ReadSaveGame()
	{
		streamReader = new StreamReader (Application.dataPath + "/TextFiles/saveGame.txt");

		saveGameNumber = int.Parse ((streamReader.ReadToEnd ()), System.Globalization.NumberStyles.Integer);

		streamReader.Close ();
	}

	public void WriteSaveGame()
	{
		streamWriter = new StreamWriter (Application.dataPath + "/TextFiles/saveGame.txt");

		streamWriter.Write(saveGameNumber);

		streamWriter.Close ();
	}
}
