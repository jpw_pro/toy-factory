﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectLevel : MonoBehaviour {

	public SaveGame saveGame;
	public Button[] levelButtons;

	public void CheckSavedLevels ()
	{
		if (saveGame.saveGameNumber >= 2)
		{
			for (int i = saveGame.saveGameNumber; (i >= 2); i--)
			{
				levelButtons[i-1].interactable = true;
			}
		}
	}
}
