﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {

	public SaveGame saveGame;
	public GameObject startGameButton;
	public GameObject continueGameButton;

	public void SetButtons()
	{
		if (saveGame.saveGameNumber==0)
		{
			startGameButton.SetActive(true);
			continueGameButton.SetActive(false);
		}
		else
		{
			startGameButton.SetActive(false);
			continueGameButton.SetActive(true);
		}
	}

	public void StartGame()
	{
		Application.LoadLevel (2);
	}

	public void ContinueGame()
	{
		Application.LoadLevel (saveGame.saveGameNumber + 1);
	}

	public void LevelSelect()
	{
		Application.LoadLevel (1);
	}
}
