﻿using UnityEngine;
using System.Collections;

public class StretchScript : MonoBehaviour {

	public GameObject[] playerSizes;
	public float[] raycastLength;
	public int currentSizeNumber = 2;
	public LayerMask stretchLayerMask;
	public GameObject raycastUpObject;
	public float[] sizeJumpPower;
	private int nextSizeNumber;

	public PlayerMovement playerMovement;

	void Update ()
	{
		KeyInput ();
	}

	public void KeyInput()
	{
		if ((Input.GetKeyDown (KeyCode.LeftShift)) && (playerMovement.currentPlayerNumber == 3))
		{
			RaycastHit rayHit;

			if (currentSizeNumber == 3)
			{
				nextSizeNumber = 0;
			}
			else
			{
				nextSizeNumber = currentSizeNumber+1;
			}
			if (currentSizeNumber ==3)
			{
				PlayerResize();
			}
			else
			{
				if (!Physics.Raycast (raycastUpObject.transform.position, Vector3.up, out rayHit, raycastLength[nextSizeNumber], stretchLayerMask))
				{
					PlayerResize();
				}
				else
				{
					if (rayHit.collider.gameObject.layer == 8)
					{
						rayHit.collider.gameObject.transform.Translate (new Vector3(0,0.5f,0));

						print ("test");

						PlayerResize();
					}
				}
			}
		}
	}

	public void PlayerResize()
	{
		playerSizes[currentSizeNumber].SetActive(false);
		
		if (currentSizeNumber < 3)
		{
			currentSizeNumber++;
		}
		else
		{
			currentSizeNumber = 0;
		}

		SizeJumpPowerSet ();
		
		playerSizes[currentSizeNumber].SetActive(true);
	}

	public void SizeJumpPowerSet()
	{
		playerMovement.jumpForce [3] = sizeJumpPower [currentSizeNumber];
	}
}
