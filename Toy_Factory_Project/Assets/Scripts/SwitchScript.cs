﻿using UnityEngine;
using System.Collections;

public class SwitchScript : MonoBehaviour {

	public Animator spikeAnimator;
	public Animator platformAnimator;
	public Renderer renderer;
	public Material switchOff;
	public Material switchOn;

	public void OnTriggerEnter(Collider other)
	{
		spikeAnimator.enabled = true;
		platformAnimator.enabled = false;
		renderer.material = switchOn;
	}
}
