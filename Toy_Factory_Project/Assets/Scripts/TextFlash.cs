﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextFlash : MonoBehaviour {

	public Text flashText;
	public bool textOn = true;
	public float time = 0;

	void Update () 
	{
		Flash ();
	}

	public void Flash ()
	{
		if (((time+1) <= Time.unscaledTime) && textOn)
		{
			flashText.enabled = false;
			textOn = false;
			time = Time.unscaledTime;
		}
		else if (((time+0.5f) <= Time.unscaledTime) && (textOn == false))
		{
			flashText.enabled = true;
			textOn = true;
			time = Time.unscaledTime;
		}
	}
}
