﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

	public PlayerMovement playerMovement;
	public bool triggered;
	public bool tutorialActive;
	public int currentTextNumber = 0;
	public int textTotalNumber;
	public GameObject[] text;
	public GameObject tutorialCanvas;
	public GameObject panel;
	public GameObject textContinue;

	void Start () 
	{
		if (triggered == false)
		{
			playerMovement.enabled = false;
			tutorialActive = true;
			Time.timeScale = 0;
		}
		else
		{
			panel.SetActive(false);
			textContinue.SetActive(false);
		}
	}

	void Update () 
	{
		if (Input.anyKeyDown && tutorialActive)
		{
			if (currentTextNumber < textTotalNumber)
			{
				text[currentTextNumber].SetActive(false);
				currentTextNumber++;
				text[currentTextNumber].SetActive(true);
			}
			else
			{
				playerMovement.enabled = true;
				tutorialActive = false;
				tutorialCanvas.SetActive(false);
				gameObject.SetActive(false);
				Time.timeScale = 1;
			}
		}
	}

	public void OnTriggerEnter (Collider other)
	{
		if (other.transform.tag == "Player")
		{
			playerMovement.enabled = false;
			text[currentTextNumber].SetActive(true);
			tutorialActive = true;

			panel.SetActive(true);
			textContinue.SetActive(true);
			Time.timeScale = 0;
		}
	}
}
