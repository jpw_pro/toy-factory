﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour {


	public Transform target;
	public Transform[] possibleTargets;
	public float pathRerequestDistance;
	public float pathTriggerDistance;
	public float wayPointBuffer;
	public float rayCastDistance;
	public float jumpForce;
	public float jumpTimeBuffer;
	public float jumpSideForce;
	public float jumpSideForceBuffer;
	public float groundForce;
	public float airForce;
	public bool canJump;
	public GameObject rayCastObject;
	public LayerMask layerMask;
	Vector3 sideJumpDirection;
	Vector3[] path;
	float lastTime = 0;
	float speed = 20;
	float distance;
	float jumpTime;
	float moveForce = 0.1f;
	int targetIndex;
	bool completedLastPath;
	bool jumping;
	bool hasJumped = false;
	bool sideJump;


	void Awake() 
	{
		path = new Vector3[1];
		path [0] = transform.position;
	}

	void Update()
	{
		if (sideJump && jumping && (Time.time >= (jumpTime + jumpSideForceBuffer)))
		{
			RaycastHit hit;
			
			if (sideJumpDirection == Vector3.left && (!Physics.Raycast (rayCastObject.transform.position, Vector3.left, out hit, rayCastDistance))) 
			{
				JumpSideForce(sideJumpDirection);
			}
			if (sideJumpDirection == Vector3.right && (!Physics.Raycast (rayCastObject.transform.position, Vector3.right, out hit, rayCastDistance)))
			{
				JumpSideForce(sideJumpDirection);
			}
		}

		if (Vector3.Distance(transform.position, target.transform.position) <= pathTriggerDistance)
		{
			distance = (Vector3.Distance((path[path.Length-1]), new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z)));

			if ((distance>pathRerequestDistance) && ((Time.time > (lastTime+0.5f)) || completedLastPath))
			{
				completedLastPath = false;
				lastTime = Time.time;
				PathRequestManager.RequestPath(transform.position,target.position, OnPathFound);
			}
		}

		float closestDistance = Vector3.Distance (transform.position, possibleTargets[0].position);
		int targetNumber = 0;
		
		for (int i=1 ; i < possibleTargets.Length ; i++)
		{
			float tempDistance = Vector3.Distance (transform.position, possibleTargets[i].position);
			
			if (tempDistance < closestDistance)
			{
				closestDistance = tempDistance;
				targetNumber = i;
			}
		}
		
		target = possibleTargets [targetNumber];

		if ((jumping == false) && (Time.time > (jumpTime + jumpTimeBuffer)))
		{
			RaycastHit hit;
		
			if ((Physics.Raycast (rayCastObject.transform.position, Vector3.left, out hit, rayCastDistance, layerMask))) 
			{
				jumping = true;
				sideJump = true;
				sideJumpDirection = Vector3.left;
			}
			if ((Physics.Raycast (rayCastObject.transform.position, Vector3.right, out hit, rayCastDistance, layerMask)))
			{
				jumping = true;
				sideJump = true;
				sideJumpDirection = Vector3.right;
			}
		}
	}

	public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
		if (pathSuccessful) {
			path = newPath;

			for (int i = 0; i < path.Length; i++)
			{
				path[i] = new Vector3 (path[i].x, transform.position.y, path[i].z);
			}

			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

	IEnumerator FollowPath() {
		Vector3 currentWaypoint = path[0];

		while (true) {
			if (Vector3.Distance(transform.position, currentWaypoint) < wayPointBuffer)
			{
				targetIndex ++;
				float timeSnap = Time.time;
				if ((targetIndex >= path.Length) || (Time.time > (timeSnap+2))) 
				{
					targetIndex = 0;
					completedLastPath = true;
					yield break;
				}

				currentWaypoint = path[targetIndex];
			}
		
			if (canJump)
			{
				if (jumping)
				{
					if (hasJumped == false)
					{
						gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

						jumpTime = Time.time;
						hasJumped = true;
					}

					if (Time.time > (jumpTime + 1))
					{
						jumping = false;
						hasJumped = false;
					}
				}
				else
				{
					Vector3 dir = (currentWaypoint - rayCastObject.transform.position).normalized;
					GetComponent<Rigidbody>().AddForce((dir*moveForce), ForceMode.Impulse);
				}
			}
			else
			{
				Vector3 dir = (currentWaypoint - rayCastObject.transform.position).normalized;
				GetComponent<Rigidbody>().AddForce((dir*moveForce), ForceMode.Impulse);
			}


			yield return null;

		}
	}

	public void JumpSideForce(Vector3 direction)
	{
		GetComponent<Rigidbody>().AddForce((direction*jumpSideForce), ForceMode.Impulse);
		sideJump = false;
	}

	public void OnDrawGizmos() {
		if (path != null) {
			for (int i = targetIndex; i < path.Length; i ++) {
				Gizmos.color = Color.black;
				Gizmos.DrawCube(path[i], Vector3.one);

				if (i == targetIndex) {
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else {
					Gizmos.DrawLine(path[i-1],path[i]);
				}
			}
		}
	}

	void OnCollisionExit (Collision other)
	{
		moveForce = airForce;
	}

	void OnCollisonEnter (Collision other)
	{
		moveForce = groundForce;
	}
}
